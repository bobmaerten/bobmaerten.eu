---
title: Rochers coco aux blancs d'œufs
date: 2017-04-15 10:21 UTC
tags: patisserie
cover: rochers_coco.jpg
---
Ceux qui me suivent sur twitter le remarquent, régulièrement dans mon flux apparaissent les photos des desserts que je réalise. Qu'on soit bien d'accord, je ne suis pas créateur de recettes, et l'intégralité des desserts que je réalise trouve son origine au hasard d'une recherche sur un moteur de recherche. Je suis interprète et non pas auteur/compositeur.

Quelques-un me demandent régulièrement les recettes, alors voila celle des rochers coco (autrement appelés congolais, mais je trouve que ça ne veut strictement rien dire), et au moins je n'aurais plus à chercher la recette très loin la prochaine fois.

READMORE

Hier soir, alors que je scrutais le contenu de mon réfrigérateur, je constate qu'il me reste des blancs d'œufs dans un bol filmé[^1]. Habituellement j'en fais des meringues[^2], mais j'étais à court de poche à douille suffisamment grande pour accueillir une telle préparation, alors je me suis décidé à achever le reste du paquet de noix de coco râpée que j'avais entamé il y a peu.s

## Recette des rochers coco

  - des blancs d'œufs
  - du sucre : 1.5 fois le poids des blancs
  - de la noix de coco râpée : 1.6 fois le poids des blancs

Pour les quantités, j'essaie de raisonner en proportions, vu qu'on ne sait jamais vraiment combien pèsent vraiment les blancs d'un oeuf et que c'est ultra-méga-pénible à peser au gramme près. En pâtisserie, la mesure est importante alors autant se baser sur la mesure la plus galère à faire.

Ensuite, mélanger le tout dans un récipient que l'on peut mettre au bain-marie[^3] et faire chauffer le tout en mélangeant régulièrement pendant 10 minutes. Pendant ce temps, préchauffer le four à 220°, chaleur tournante.

Prendre de petites poignées de la pâte chauffée et en faire des tas en forme de cône sur une plaque. Peut importe la taille mais essayez d'être régulier sur l'ensemble de la fournée, afin que la cuisson soit uniforme.

Mettre au four pour 6 à 12 minutes en fonction de la taille des rochers. 6 minutes pour les tout petits de la taille d'une noix, 12 pour les gros de la taille d'une grosse tasse à café. Dans tous les cas, surveillez la cuisson et les sortir du four une fois la "bonne" couleur atteinte (voir la photo). Autant pour les mesures il faut être précis, autant pour les cuissons la plupart du temps, c'est à l'œil.

Je précise au cas où que ça se conserve quelques jours dans une boîte métallique, mais bon, ça reste rarement très longtemps ces choses là…

Bon appétit !

[^1]: Eh oui, les blancs se conservent une dizaine de jours au frais à la différence des jaunes qu'il faut utiliser tout de suite.
[^2]: Allez, hop cadeau, les proportions pour les meringues, c'est 2 fois le poids des blancs en sucres. Oui sucres au pluriel, on utilise du sucre en poudre et du sucre glace, mais bon, ce sera l'opportunité d'un autre billet tiens…
[^3]: Mettre le récipient dans une casserole de plus grande taille, remplie d'eau à moitié, sur le gaz/vitro/induction moyen, de manière à ce que le fond du récipient trempe dans l'eau, sans pour autant faire bouillir l'eau de la casserole.
