---
title: Ouverture du blog technique de Level UP Solutions
date: 2017-04-12 18:49 UTC
tags: blog, work, dev, devops
cover: levelupsolutions.png
---
Depuis le temps que cela nous trottait dans la tête, ça y est, on s'est enfin lancé.

READMORE

Le [blog technique](https://levups.com/fr/blog/index.html) de [Level UP Solutions](https://levups.com) est ouvert depuis le 17 mars 2017 ! Nous y posons là bas les choses sur lesquelles nous avons planché et qui nous paraissent suffisamment intéressantes ou utiles au plus grand nombre, en espérant qu'il puisse dépanner ceux qui tomberont dessus par "hasard" (merci le référencement naturel) et pourquoi pas nous faire connaitre un peu plus au passage. Bref, tout comme ce blog. 😜

Déjà au programme, un article très intéressant sur l'[optimisation de Ruby sur nos machines et serveurs](https://levups.com/fr/blog/2017/ruby_jemalloc_paas_heroku_scalingo.html), testé et validé par [Adrien Jarthon](https://medium.com/@adrienjarthon/ruby-jemalloc-results-for-updown-io-d3fb2d32f67f) de UpDown.io. Et [le petit piaf 🐦](https://twitter.com/levups/status/850664605101809664) me chuchote que ça risque de parler de Crystal dans pas longtemps…

Oh, ai-je précisé que nos articles sont aussi [disponible en anglais](https://levups.com/en/blog/) ?
