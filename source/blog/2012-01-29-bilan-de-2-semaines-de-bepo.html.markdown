---
title: Bilan de 2 semaines de bépo
cover: map.jpg
tags:  bépo, materiel
---

Lors de la veille que j'ai effectuée avant de me mettre au bépo, je suis bien évidemment tombé sur [Ploum](http://ploum.net), qui a passé le pas il y a bien des années déjà, et qui fait figure (enfin, pour ma part) d'ambassadeur dans le domaine.

Je relisais ce matin [un article de ses journaux (linuxfr)](http://linuxfr.org/users/ploum/journaux/2-semaines-en-typematrix-b%C3%A9po) qui expose sa progression dans l'apprentissage de la manipulation du bépo, et j'ai été vraiment épaté par les similarités assez précises dans mon ressenti au bout de ces 2 semaines de pratique [quasi](https://twitter.com/#!/bobmaerten/status/161467232432553985)-exclusive (ce post y compris, bien entendu).

Je me permets de copier/coller son ressenti, car aussi étonnant que cela puisse paraître, c'est *exactement* ce qui m'est arrivé pour le moment depuis 15 jours (hormis le digicode).

Je cite Ploum, dans un journal Linuxfr :

> Semaine 1 (entre 10 et 20 MPM) :
> C'est très difficile. On a tendance a raccourcir ses phrases et à taper lettre à lettre. La frustration est grande de ne plus avoir taper ses idées, le clavier est un handicap. Je suis même devenu un peu parano : mon digicode et mon gsm me semblaient en bépo !

> Semaine 2 (entre 20 et 30 MPM) :
> On peut commencer à taper. Cela fait plaisir. Parfois on tape très très vite et on fait des fautes toutes les deux lettres car on commence à passer en mode automatique mais sans la maîtrise. Parfois, en cas de fatigue, les doigts vont instinctivement chercher la position Azerty et on se retrouve à taper en regardant le clavier, par simple réflexe. On recommence à répondre à ses mails et à revenir à la vie en ligne.

Alors je sais pas comment celà évoluera, mais toujours est-il que ce me conforte dans mes impressions, et que ça tombe à pic alors que je commençais en fin de semaine à désespérer de me voir progesser aussi peu.

Et vous, lecteurs bépoistes, qu'en est (était) il de vos impressions et progrès dans les premières semaines ?
