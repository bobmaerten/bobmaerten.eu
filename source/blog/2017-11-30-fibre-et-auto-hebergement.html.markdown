---
title: Fibre et auto-hébergement
date: 2017-11-30 11:24 UTC
tags: blog, life
cover: fibre.jpg
---
Après des années passées à ramer avec un ADSL plafonnant les bons jours à 6Mbit/s, j'ai sauté sur la proposition d'un opérateur local que je suivais de près depuis son annonce de test de raccordement pour les particuliers.

READMORE

## Installation

![Logo Techcrea Solutions](2017-11-30-fibre-et-auto-hebergement/logo-techcrea.png)

En effet, la société [Techcrea Solutions](https://www.techcrea.fr/) fibre les entreprises de la région Valenciennoise, mais s'est également ouverte aux particuliers. J'ai alors sauté sur l'opportunité !

Outre l'installation du câble aérien provenant de la boite de multiplexage la plus proche, la mise en place au domicile est une entreprise délicate. Perçage du mur, installation du boitier, soudure des fibres et installation du routeur. Ce ne fût pas une mince affaire, malgré l'expérience des techniciens chaque maison a ses particularités, comme la mienne dont l'épaisseur des murs est supérieure à la taille de la mèche de la perceuse...

![Installation](2017-11-30-fibre-et-auto-hebergement/installation.jpg)

Mais au final, le résultat est là. Du gigabit symétrique à la maison :

![Débits](2017-11-30-fibre-et-auto-hebergement/debits.jpg)

## Internet au quotidien

Pour tout ce qui est usage au quotidien, c'est évidemment le jour et la nuit. De la bande passante pour mon usage professionnel, tout en permettant aux autres membres de la famille de regarder une série en 4K sur Netflix ou Youtube, avec encore bien suffisamment sous la pédale pour les grosses mises à jours des jeux Blizzard/Steam. Fini les réservations de créneau pour la bande passante, on se fait **très** rapidement à ce nouveau confort !

Bref, il était temps que ce genre d'évolution arrive hors des grandes agglomérations et je remercie bien chaleureusement toute l'équipe de Techcrea Solutions pour leur formidable initiative.

## Auto-hébergement

Rapidement, les vieux réflexes reviennent. Du temps lointain des prémisses du déploiement de l'ADSL ou j'avais mon propre serveur mail à la maison, c'est ici un NAS Synology qui fait office de serveur à tout faire, et en particulier son service web avec lequel je voulais servir les pages de ce blog statiquement généré.

Quelle ne fut pas ma déconvenue quelques heures après la mise en place de constater les piètres performances de l'engin.

### S3
![Performances S3](2017-11-30-fibre-et-auto-hebergement/avant.jpg)

### NAS à la maison
![Performances NAS](2017-11-30-fibre-et-auto-hebergement/apres.jpg)

Alors évidement, l'architecture, les CDNs, le cache, la performance brute, tout ceci n'est pas directement comparable, mais je ne m'attendais pas à rapport x4 tout de même... Cela dit, pour un blog perso essentiellement lu au travers de quelques lecteurs RSS, ça fera bien l'affaire. Et puis ça me donne des billes pour optimiser voire même carrément revoir le thème de ce blog au passage.

**Update du 1er décembre 2017** 

Il semblerait que le NAS réponde un peu mieux depuis ce jour, notamment en France où on obtient des valeurs tout à fait honorable de l'ordre de 300ms. Pour le reste, c'est également cohérent vu que le site ne dispose plus Cloudfront qui permettait de proposer le site sur de CDN de proximité.

![Performances NAS update](2017-11-30-fibre-et-auto-hebergement/apres_update.jpg)

