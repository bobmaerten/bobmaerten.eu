---
title: Deux nouvelles pages sur le site
date: 2019-03-31 12:12 UTC
tags: blog
cover:
---

Le blog s'étoffe d'une nouvelle page « [/uses](/uses) » qui reprend la tendance initiée par [Wes Bos](https://wesbos.com) sur son site afin de répertorier les outils et logiciels utliisés au quotidien. J'essaierai de la compléter ou de faire des billets individuels sur les choix opérés.

Une autre page « [/now](/now) » (initié par [Derek Sivers](https://sivers.org/nowff)) remplace la sempiternelle « À propos » pour décrire ce que je fais à un instant `t`, plutôt que d'essayer de trouver une vague pseudo-descrition de type *curriculum vitae* qui n'interesse personne.
